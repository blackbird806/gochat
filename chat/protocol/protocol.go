package protocol

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
)

type(

	PacketType uint8

	Header struct {
		PType    PacketType
		DataSize uint32 	// move into packet
	}

	Packet struct {
		Header Header
		Data []byte
	}

	ConnectionPacket struct {
		Header Header
		Data []byte
	}

)

const(
	Connection PacketType = iota
	Message
)

func NewPacket(t PacketType, in_data []byte) *Packet {

	h := Header{
		PType:    t,
		DataSize: uint32(len(in_data)),
	}

	return &Packet{
		Header: h,
		Data: in_data,
	}
}

func (p *Packet) toBinary() (*bytes.Buffer, error) {

	buf := new(bytes.Buffer)
	err := binary.Write(buf, binary.LittleEndian, p.Header.DataSize)
	if err != nil {
		return nil, err
	}
	err = binary.Write(buf, binary.LittleEndian, p.Header.PType)
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(p.Data); i++ {
		err = binary.Write(buf, binary.LittleEndian, p.Data[i])
		if err != nil {
			return nil, err
		}
	}
	return buf, nil
}

func (p *Packet) Serialize(writer io.Writer) (err error) {

	buf, err := p.toBinary()
	_, err = writer.Write(buf.Bytes())
	if err != nil{
		return
	}

	return nil
}

func UnSerialize(reader io.Reader) (*Packet, error) {
	buffer := make([]byte, 0)
	_, err := reader.Read(buffer)
	if err != nil {
		return nil, err
	}
	packet := fromBinary(bytes.NewBuffer(buffer))

	return packet, nil
}

func fromBinary(buf *bytes.Buffer) *Packet {

	p := NewPacket(Message, make([]byte, 0))

	read := bytes.NewReader(buf.Bytes())
	err := binary.Read(read, binary.LittleEndian, &p.Header.DataSize)
	if err != nil {
		fmt.Println(err)
	}

	err = binary.Read(read, binary.LittleEndian, &p.Header.PType)
	if err != nil {
		fmt.Println(err)
	}

	p.Data = make([]byte, p.Header.DataSize)
	for i := 0; uint32(i) < p.Header.DataSize; i++ {
		err = binary.Read(read, binary.LittleEndian, &p.Data[i])
		if err != nil {
			fmt.Println(err)
		}
	}
	return p
}
