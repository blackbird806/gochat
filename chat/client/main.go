package main

import (
	. "gochat/chat/protocol"
	"bufio"
	"errors"
	"executor"
	"fmt"
	"log"
	"net"
	"os"
	"strings"
	"time"
)

type(
	Server struct {
		ip string
	}

	Client struct {
		ip string
		pseudo string
		conn net.Conn
	}
)

// https://stackoverflow.com/questions/23558425/how-do-i-get-the-local-ip-address-in-go
func externalIP() (string, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return "", err
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return "", err
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			return ip.String(), nil
		}
	}
	return "", errors.New("are you connected to the network?")
}

//https://stackoverflow.com/questions/48798588/how-do-you-remove-the-first-character-of-a-string
func trimLeftChars(s string, n int) string {
	m := 0
	for i := range s {
		if m >= n {
			return s[i:]
		}
		m++
	}
	return s[:0]
}

// https://stackoverflow.com/questions/8689425/remove-last-character-of-a-string
func trimSuffix(s, suffix string) string {
	if strings.HasSuffix(s, suffix) {
		s = s[:len(s)-len(suffix)]
	}
	return s
}

func CreateClient() *Client {

	addr, _ := externalIP()
	return &Client {
		ip: addr,
		pseudo: "billy",
	}
}

func (self *Client) sendStrToServer(s string) {
	_, err := fmt.Fprintf(self.conn, s)
	if err != nil {
		logger.Println("str Send Error")
	}
}

func (self *Client) sendPacketToServer(p *Packet) (err error) {
	err = p.Serialize(self.conn)
	return
}

func (self *Client) ListenServer() {
	for {
		message, _ := bufio.NewReader(self.conn).ReadString('\n')
		logger.Print("Message from Server : ", message)
	}
}

func (self *Client) WaitUserInputs() {

	for {
		reader := bufio.NewReader(os.Stdin)
		logger.Print("Text to send : ")
		userText, inputErr := reader.ReadString('\n')

		if inputErr != nil {
			logger.Println("input error")
			return
		}

		if strings.HasPrefix(userText, "/") {
			command := trimLeftChars(userText, 1)
			command = trimSuffix(command, "\n")
			self.processUserCommands(command)
			continue
		}

		sendedMsg := userText + "\n"
		err := self.sendPacketToServer(NewPacket(Message, []byte(sendedMsg)))
		if err != nil{
			logger.Println(err)
		}
		//bytes, err := fmt.Fprintf(self.conn, sendedMsg)
	}
}

func (self *Client) processUserCommands(command string) {
	switch command {
		case "disconnect" :
			// TODO
		case "getIp" :
			logger.Println("current ip : ", self.ip)
	}
}

func (self *Client) ConnectToServer(server Server) {

	conn, connErr := net.Dial("tcp", server.ip)
	self.conn = conn

	if connErr != nil {
		logger.Println("Server Connection Error")
		logger.Println("trying to reconnect ...")
		time.Sleep(5 * time.Second)
		self.ConnectToServer(server)
		return
	}

	dataSend := make([]byte, 128)
	// https://stackoverflow.com/questions/8039245/convert-string-to-fixed-size-byte-array-in-go
	copy(dataSend[:], self.pseudo)
	connectionRequest := NewPacket(Connection, dataSend)
	err := self.sendPacketToServer(connectionRequest)
	if err != nil{
		logger.Println(err)
	}
	logger.Println("connected successfully to server : ", server.ip)
}

var logger = log.New(os.Stdout, "", log.LstdFlags)

func main() {

	exec := executor.NewExecutor(5, 100)

	logger.Println("client starting")
	self := CreateClient()

	server := Server{"127.0.0.1:8081"}
	logger.Println("current ip : ", self.ip)
	self.ConnectToServer(server)
	//listen := self.ListenServer
	//exec.AddTask(listen)

	self.WaitUserInputs()

	exec.Shutdown()
	exec.Wait()
}
