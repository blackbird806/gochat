package main

import (
	"bufio"
	"fmt"
	. "gochat/chat/protocol"
	"net"
)

type (

	Client struct {
	pseudo string
	ip string
	conn net.Conn
	sendQueue chan *Packet
	}

	Server struct {
	port    string
	conn    net.Listener
	clients map [string] *Client
	}

)

func CreateServer() *Server {
	return &Server{
		port:    ":8081",
		conn:    nil,
		clients: make(map[string]*Client),
	}
}
/*
func (s *Server) WaitForPacket() *Packet {

	fmt.Println("client IP: ", s.conn.RemoteAddr())
	//reader := bufio.NewReader(s.conn)
	data := make([]byte, unsafe.Sizeof(Packet{}))
	_, requestError := io.ReadFull(reader, data)

	connectionPacket := UnSerialize(bytes.NewBuffer(data))
	fmt.Println(string(connectionPacket.Data[:connectionPacket.Header.DataSize]))
	if requestError != nil {
		fmt.Println("request error : ", requestError)
	}

	return  connectionPacket
}
*/

/*
func (s *Server) ProcessConnectionRequests() {

	ln, listenErr := net.Listen("tcp", s.port)
	if listenErr != nil {
		fmt.Println("listen error", listenErr)
		return
	}
	conn, connErr := ln.Accept()
	if connErr != nil {
		fmt.Println("connection error", connErr)
		return
	}
	s.conn = conn

}
*/
func (s *Server) RegisterNewClient(pseudo, ip string) (*Client, error) {

	if _, ok := s.clients[pseudo]; ok {
		return nil, fmt.Errorf("client already registered")
	}
	tmp := &Client{pseudo:pseudo, ip: ip}
	s.clients[pseudo] = tmp

	return tmp, nil
}

func (s *Server) Run() {

	ln, listenErr := net.Listen("tcp", s.port)
	if listenErr != nil {
		fmt.Println("listen error", listenErr)
		return
	}
	s.conn = ln

	for {
		conn, connErr := ln.Accept()
		if connErr != nil {
			fmt.Println("connection error", connErr)
			return
		}
		ioRead := bufio.NewReader(conn)

		//TODO REGISTER NEW CLIENT
		// START CLIENT ROUTINES (READ / WRITE)

		fmt.Println("client IP: ", conn.RemoteAddr())

		packet, err := UnSerialize(ioRead)
		if err != nil {
			fmt.Println("read error", err)
			break
		}

		switch packet.Header.PType {
		case Connection :
			fmt.Println("connection")
		case Message :
			fmt.Println("message")
		}
		fmt.Println(string(packet.Data))
	}
}

var dispatchChan = make(chan string, 10) //Buffered channel to avoid write blocking

func main() {
	s := CreateServer()
	s.Run()
}
